from matplotlib import pyplot as plt
import numpy as np

class PlotBuilder():
    def __init__(self, directory: str, dt: float, dr: float, distinguish: str):
        self.dir = directory
        self.dt = dt
        self.dr = dr
        self.distinguish = distinguish
        self.ions_Ntot = np.genfromtxt(f'{self.dir}/ions_Ntot.txt')
        self.electrons_Ntot = np.genfromtxt(f'{self.dir}/electrons_Ntot.txt')
        self.electrons_mean_energy = np.genfromtxt(f'{self.dir}/electrons_mean_energy.txt')
        self.ions_mean_energy = np.genfromtxt(f'{self.dir}/ions_mean_energy.txt')
        
    def get_Ntot_plot(self):
        if (_plt == None):
            plt.plot(self.electrons_Ntot[:, 0] * self.dt * 1e9, self.electrons_Ntot[:, 1], label=f'Число электронов {self.distinguish}')
            plt.plot(self.ions_Ntot[:, 0] * self.dt * 1e9, self.ions_Ntot[:, 1], label=f'Число ионов {self.distinguish}')
            plt.xlabel('t, нс')
            plt.ylabel('Число суперчастиц')
            plt.legend()
            return plt
        else:
            _plt.plot(self.electrons_Ntot[:, 0] * self.dt * 1e9, self.electrons_Ntot[:, 1], label=f'Число электронов {self.distinguish}')
            _plt.plot(self.ions_Ntot[:, 0] * self.dt * 1e9, self.ions_Ntot[:, 1], label=f'Число ионов {self.distinguish}')
            _plt.xlabel('t, нс')
            _plt.ylabel('Число суперчастиц')
            _plt.legend()
            return _plt
        
    def get_mean_energy_plot(self):
        if (_plt == None):
            plt.plot(self.electrons_mean_energy[:, 0] * self.dt * 1e9, self.electrons_mean_energy[:, 1], label=f'Энергия электронов {self.distinguish}')
            plt.plot(self.ions_mean_energy[:, 0] * self.dt * 1e9, self.ions_mean_energy[:, 1], label=f'Энергия ионов {self.distinguish}')
            plt.xlabel('t, нс')
            plt.ylabel('Энергия суперчастиц, эВ')
            plt.legend()
            return plt
        else:
            _plt.plot(self.electrons_mean_energy[:, 0] * self.dt * 1e9, self.electrons_mean_energy[:, 1], label=f'Энергия электронов {self.distinguish}')
            _plt.plot(self.ions_mean_energy[:, 0] * self.dt * 1e9, self.ions_mean_energy[:, 1], label=f'Энергия ионов {self.distinguish}')
            _plt.xlabel('t, нс')
            _plt.ylabel('Энергия суперчастиц, эВ')
            _plt.legend()
            return _plt
    
    def get_average_potential_profile(self, start: int, stop: int, step: int, scale: int):
        pot_dir = f'{self.dir}/phi_hist/'
        pot_tmp = np.genfromtxt(f'{pot_dir}phi{start}.txt')
        num_iter = (stop - start) / step
        Nx, Ny = pot_tmp.shape
        averaged_pot = np.zeros(Nx)
        for i in range(start, stop, step):
            pot = np.genfromtxt(f'{pot_dir}phi{i}.txt')
            averaged_pot += pot[Ny // 2, :]
        averaged_pot /= num_iter
        plt.plot([i * self.dr * 100 / scale for i in range(len(averaged_pot[Nx//2:]))], averaged_pot[Nx//2:], label=f'Профиль потенциала {self.distinguish}')
        plt.xlabel('R, см')
        plt.ylabel('V, В')
        plt.legend()
    
    def get_average_ions_dencity_profile(self, start: int, stop: int, step: int, scale: int):
        rho_i_dir = f'{self.dir}/rho_i_hist/'
        rho_i_tmp = np.genfromtxt(f'{rho_i_dir}rho_i_{start}.txt')
        Nx, Ny = rho_i_tmp.shape
        averaged_rho_i = np.zeros(Nx)
        for i in range(start, stop, step):
            rho_i = np.genfromtxt(f'{rho_i_dir}rho_i_{i}.txt')
            averaged_rho_i += rho_i[Ny // 2, :]
        averaged_rho_i /= ((stop - start) / step)
        plt.plot([i * self.dr * 100 / scale for i in range(len(averaged_rho_i[Nx//2:]))], averaged_rho_i[Nx//2:] / (1.6e-19), label=f'Профиль плотности ионов {self.distinguish}')
        plt.xlabel('R, см')
        plt.ylabel('n_i, м^-3')
        plt.legend()
        
    def get_average_electrons_dencity_profile(self, start: int, stop: int, step: int, scale: int):
        rho_e_dir = f'{self.dir}/rho_e_hist/'
        rho_e_tmp = np.genfromtxt(f'{rho_e_dir}rho_e_{start}.txt')
        Nx, Ny = rho_e_tmp.shape
        averaged_rho_e = np.zeros(Nx)
        for i in range(start, stop, step):
            rho_e = np.genfromtxt(f'{rho_e_dir}rho_e_{i}.txt')
            averaged_rho_e += rho_e[Ny // 2, :]
        averaged_rho_e /= ((stop - start) / step)
        plt.plot([i * self.dr * 100 / scale for i in range(len(averaged_rho_e[Nx//2:]))], averaged_rho_e[Nx//2:] / (-1.6e-19), label=f'Профиль плотности электронов {self.distinguish}')
        plt.xlabel('R, см')
        plt.ylabel('n_e, м^-3')
        plt.legend()